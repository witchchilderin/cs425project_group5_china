import Mock from "mockjs";

const baseUrl = "http://localhost:8088";

Mock.mock(baseUrl + "/user/item", {
  name: "Guangwen LIAO",
  email: "liaoguangwen192@163.com",
  phone: "18680345680",
});

Mock.mock(baseUrl + "/user/login", {
  code: 200,
  data: {
    account: "liaogw",
    password: "123",
  },
  message: "ok",
});

Mock.mock(baseUrl + "/user/list", {
  code: 200,
  data: [
    {
      customerId: "@id",
      email: "liaoguangwen@163.com",
      firstName: "guangwen",
      lastName: "liao",
      phone: "18680345680",
    },
    {
      customerId: "@id",
      email: "yuye@163.com",
      firstName: "ye",
      lastName: "yu",
      phone: "18680345789",
    },
    {
      customerId: "@id",
      email: "wuwei@163.com",
      firstName: "wei",
      lastName: "wu",
      phone: "18680345569",
    },
  ],
  message: "ok",
});

/**
 * 航班查询接口
 */
Mock.mock(baseUrl + "/flights/list", {
  code: 200,
  data: [
    {
      flight_id: "1",
      airline_code: "BY737",
      aircraft_id: 13,
      flight_number: "f123",
      departure_time: "2021-05-05 08:40",
      departure_airport: "dairport",
      arrival_time: "2021-05-09 09:25",
      departure_gate: "g31",
      first_class_capacity: 10,
      economy_class_capacity: 32,
      first_class_price: 120.54,
      economy_class_price: 100.32,
      miles: 840,
    },
    {
      flight_id: "2",
      airline_code: "BY737",
      aircraft_id: 13,
      flight_number: "f456",
      departure_time: "2021-05-05 08:40",
      departure_airport: "dairport",
      arrival_time: "2021-05-09 09:25",
      departure_gate: "g28",
      first_class_capacity: 10,
      economy_class_capacity: 32,
      first_class_price: 150.54,
      economy_class_price: 120.32,
      miles: 540,
    },
    {
      flight_id: "3",
      airline_code: "BY737",
      aircraft_id: 13,
      flight_number: "f789",
      departure_time: "2021-05-05 08:40",
      departure_airport: "dairport",
      arrival_time: "2021-05-09 09:25",
      departure_gate: "g49",
      first_class_capacity: 10,
      economy_class_capacity: 32,
      first_class_price: 180.54,
      economy_class_price: 150.32,
      miles: 970,
    },
  ],
  message: "ok",
});
