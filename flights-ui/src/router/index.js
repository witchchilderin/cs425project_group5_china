import Vue from "vue"
import VueRouter from "vue-router"
import Home from "../views/Home"
import Login from "../views/Login"
import Register from "../views/Register"
import Flights from "../views/Flights"
import Lost from "../views/Lost"
import Dashboard from "../views/Dashboard"
import Personal from "../views/Personal"
import Orders from "../views/Orders"
import Booking from "../views/Booking"
import Seats from '@/views/Seats'
import Payment from '@/views/Payment'

Vue.use(VueRouter);

const routes = [{
		path: "/",
		name: "Home",
		component: Home,
		children: [{
				path: "flights",
				name: "Flights",
				component: Flights,
			},
			{
				path: "/dashboard",
				name: "Dashboard",
				component: Dashboard,
			},
			{
				path: "/booking",
				name: "booking",
				component: Booking,
			},
			{
				path: "/seats",
				name: "seats",
				component: Seats,
			},
			{
				path: "/orders",
				name: "orders",
				component: Orders,
			},
			{
				path: "/payment",
				name: "payment",
				component: Payment,
			},
			{
					path: "personal",
					name: "Personal",
					component: Personal,
			}
		],
	},
	{
		path: "/login",
		name: "Login",
		component: Login,
	},
	{
		path: "/register",
		name: "Register",
		component: Register,
	},

	{
		path: "/*",
		name: "Lost",
		component: Lost /* () => import('../views/About.vue') */ ,
	},
];

const router = new VueRouter({
	//mode: "history",
	base: process.env.BASE_URL,
	routes,
});

//路由守卫设置，登录界面登录成功之后，会把用户信息保存在会话中
router.beforeEach((to, from, next) => {
	let userName = sessionStorage.getItem("user");

	if (to.path === "/login") {
		//如果访问登录页面，用户会话存在，已登录，则跳转到主页
		// if (userName) {
		// 	next({
		// 		path: "/"
		// 	})
		// } else {
		// 	next()
		// }
		next()
	} else {
		//访问非登录页面，会话信息不存在，代表未登录，则跳转到登录页面
		if (!userName) {
			if (from.path === "/login") {
				next()
			} else {
				next({
					path: "/login"
				})
			}
		} else {
			//
			next();
		}
	}
});

export default router;
