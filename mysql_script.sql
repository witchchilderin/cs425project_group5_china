/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2021/5/4 17:46:12                            */
/*==============================================================*/


drop table if exists Address;

drop table if exists Aircraft;

drop table if exists Airline;

drop table if exists Airport;

drop table if exists Booking;

drop table if exists BookingDetail;

drop table if exists Cabin;

drop table if exists City;

drop table if exists Contacts;

drop table if exists CreditCard;

drop table if exists Customer;

drop table if exists CustomerMileage;

drop table if exists DepartureGate;

drop table if exists Flight;

drop table if exists MileagePlan;

drop table if exists Payment;

drop table if exists Price;

drop table if exists Ticket;

drop table if exists opening;

/*==============================================================*/
/* Table: Address                                               */
/*==============================================================*/
create table Address
(
   address_id           int not null auto_increment comment '地址编号',
   customer_id          int not null comment '客户编号',
   card_code            varchar(30) comment '信用卡号',
   state_code           char(2) not null comment '所属州名',
   zip_code             numeric(5,0) not null comment '邮政编码',
   address_name         varchar(255) not null comment '地址名称',
   creation_date        timestamp not null comment '创建日期',
   primary key (address_id)
);

alter table Address comment '地址';

/*==============================================================*/
/* Table: Aircraft                                              */
/*==============================================================*/
create table Aircraft
(
   aircraft_id          int not null auto_increment comment '飞机编号',
   airline_code         char(2) not null comment '航空公司编号',
   aircraft_type        varchar(30) not null comment '飞机类型',
   first_class_capacity int not null comment '头等舱容量',
   economy_class_capacity int not null comment '经济舱容量',
   description          text comment '描述',
   primary key (aircraft_id)
);

alter table Aircraft comment '飞机';

/*==============================================================*/
/* Table: Airline                                               */
/*==============================================================*/
create table Airline
(
   airline_code         char(2) not null comment '航空公司编号',
   airline_full_name    varchar(255) comment '航空公司全名',
   country              varchar(10) comment '所属国家',
   creation_date        timestamp comment '创建日期',
   primary key (airline_code)
);

alter table Airline comment '航空公司';

/*==============================================================*/
/* Table: Airport                                               */
/*==============================================================*/
create table Airport
(
   IATA_code            char(3) not null comment '机场编号',
   city_code            varchar(10) not null comment '城市编号',
   airport_name         varchar(50) not null comment '机场名称',
   status               int comment '状态  0:可飞 1停飞',
   primary key (IATA_code)
);

alter table Airport comment '机场';

/*==============================================================*/
/* Table: Booking                                               */
/*==============================================================*/
create table Booking
(
   order_id             varchar(30) not null comment '订单编号',
   customer_id          int not null comment '客户编号',
   pay_no               varchar(30) comment '支付编号',
   status               int not null comment '状态  0:可飞 1停飞',
   departure_time       timestamp comment '起飞时间',
   arrival_time         timestamp comment '到达时间',
   departure_city       varchar(20) comment '起飞城市',
   arrival_city         varchar(20) comment '到达城市',
   creation_time        timestamp comment '创建日期',
   total_price          numeric(10,0) not null comment '总价格',
   primary key (order_id)
);

alter table Booking comment '预订';

/*==============================================================*/
/* Table: BookingDetail                                         */
/*==============================================================*/
create table BookingDetail
(
   detail_id            int not null auto_increment comment '订单详情编号',
   contacts_id          int not null comment '联系人编号',
   order_id             varchar(30) not null comment '订单编号',
   flight_id            int not null comment '航班ID',
   class_level          int not null comment '舱位',
   seat_no              varchar(5) not null comment '座位号',
   phone                varchar(15) comment '电话',
   first_name           varchar(30) not null comment '姓',
   last_name            varchar(30) not null comment '名',
   price                numeric(10,0) not null comment '价格',
   primary key (detail_id)
);

alter table BookingDetail comment '详情';

/*==============================================================*/
/* Table: Cabin                                                 */
/*==============================================================*/
create table Cabin
(
   cabin_id             int not null auto_increment comment '舱位ID',
   aircraft_id          int not null comment '飞机编号',
   number               varchar(5) comment '舱位编号',
   description          text comment '描述',
   primary key (cabin_id)
);

alter table Cabin comment '飞机舱位';

/*==============================================================*/
/* Table: City                                                  */
/*==============================================================*/
create table City
(
   city_code            varchar(10) not null comment '城市编号',
   city_name            varchar(20) not null comment '城市名称',
   country              varchar(10) not null comment '所属国家',
   state_code           char(2) not null comment '所属州名',
   primary key (city_code)
);

alter table City comment '城市';

/*==============================================================*/
/* Table: Contacts                                              */
/*==============================================================*/
create table Contacts
(
   contacts_id          int not null auto_increment comment '联系人编号',
   customer_id          int not null comment '客户编号',
   id_type              int comment 'ID类型',
   id_number            varchar(9) comment 'ID编号',
   phone                varchar(15) not null comment '电话',
   email                varchar(50) comment '邮箱',
   first_name           varchar(30) not null comment '姓',
   last_name            varchar(30) not null comment '名',
   gender               int comment '性别',
   birth                date comment '生日',
   description          text comment '描述',
   primary key (contacts_id)
);

alter table Contacts comment '联系人';

/*==============================================================*/
/* Table: CreditCard                                            */
/*==============================================================*/
create table CreditCard
(
   card_code            varchar(30) not null comment '信用卡号',
   address_id           int not null comment '地址编号',
   customer_id          int not null comment '客户编号',
   bank                 varchar(100) not null comment '所属银行',
   effective_date       timestamp comment '开卡日期',
   expiration_date      timestamp comment '失效日期',
   primary key (card_code)
);

alter table CreditCard comment '信用卡';

/*==============================================================*/
/* Table: Customer                                              */
/*==============================================================*/
create table Customer
(
   customer_id          int not null auto_increment comment '客户编号',
   last_name            varchar(30) not null comment '名',
   first_name           varchar(30) not null comment '姓',
   ssn                  numeric(9,0) comment '社保号',
   driving_num          numeric(9,0) comment '驾驶证号',
   passport             varchar(20) comment '护照',
   email                varchar(50) comment '邮箱',
   phone                varchar(15) not null comment '电话',
   password             varchar(30) not null comment '密码',
   home_airport         char(3) comment '机场',
   gender               int not null comment '性别',
   birth                date comment '生日',
   description          text comment '描述',
   primary key (customer_id)
);

alter table Customer comment '客户';

/*==============================================================*/
/* Table: CustomerMileage                                       */
/*==============================================================*/
create table CustomerMileage
(
   cm_id                int not null auto_increment comment '用户里程编号',
   customer_id          int not null comment '客户编号',
   airline_code         char(2) not null comment '航空公司编号',
   totalMileage         numeric(10,0) comment '总里程数',
   primary key (cm_id)
);

alter table CustomerMileage comment '客户里程';

/*==============================================================*/
/* Table: DepartureGate                                         */
/*==============================================================*/
create table DepartureGate
(
   gate_number          varchar(10) not null comment '登机口编号',
   IATA_code            char(3) not null comment '机场编号',
   description          text comment '描述',
   primary key (gate_number)
);

alter table DepartureGate comment '登机口';

/*==============================================================*/
/* Table: Flight                                                */
/*==============================================================*/
create table Flight
(
   flight_id            int not null auto_increment comment '航班ID',
   airline_code         char(2) not null comment '航空公司编号',
   aircraft_id          int not null comment '飞机编号',
   flight_number        varchar(10) not null comment '航班编号',
   departure_time       timestamp comment '起飞时间',
   arrival_time         timestamp comment '到达时间',
   departure_airpot     char(3) comment '起飞机场',
   arrival_airport      char(3) comment '到达机场',
   departure_gate       varchar(10) not null comment '起飞登机口',
   first_class_capacity int comment '头等舱容量',
   economy_class_capacity int comment '经济舱容量',
   first_class_price    numeric(10,0) comment '头等舱价格',
   economy_class_price  numeric(10,0) comment '经济舱价格',
   miles                numeric(10,0) comment '里程数',
   primary key (flight_id)
);

alter table Flight comment '航班';

/*==============================================================*/
/* Table: MileagePlan                                           */
/*==============================================================*/
create table MileagePlan
(
   mileage_no           int not null comment '里程编号',
   airline_code         char(2) not null comment '航空公司编号',
   mileage_level        int not null comment '里程等级',
   miles                numeric(10,0) not null comment '里程数',
   bonus                text comment '奖励',
   primary key (mileage_no)
);

alter table MileagePlan comment '里程计划';

/*==============================================================*/
/* Table: Payment                                               */
/*==============================================================*/
create table Payment
(
   pay_no               varchar(30) not null comment '支付编号',
   card_code            varchar(30) not null comment '信用卡号',
   order_id             varchar(30) not null comment '订单编号',
   payment_mode         varchar(20) not null comment '支付方式',
   status               int not null comment '状态  0:可飞 1停飞',
   creation_time        timestamp not null comment '创建日期',
   payment_time         timestamp comment '支付日期',
   primary key (pay_no)
);

alter table Payment comment '支付';

/*==============================================================*/
/* Table: Price                                                 */
/*==============================================================*/
create table Price
(
   price_id             int not null auto_increment comment '编号',
   flight_id            int not null comment '航班ID',
   class_level          int not null comment '舱位',
   cost                 numeric(10,0) not null comment '价格',
   discount             numeric(2,0) comment '折扣',
   primary key (price_id)
);

alter table Price comment '价格';

/*==============================================================*/
/* Table: Ticket                                                */
/*==============================================================*/
create table Ticket
(
   ticket_id            int not null comment '机票编号',
   order_id             varchar(30) not null comment '订单编号',
   airline_code         char(2) not null comment '航空公司编号',
   airline_full_name    varchar(255) comment '航空公司全名',
   flight_id            int not null comment '航班ID',
   flight_number        varchar(10) not null comment '航班编号',
   departure_time       timestamp not null comment '起飞时间',
   arrival_time         timestamp not null comment '到达时间',
   departure_airport    char(3) comment '起飞机场',
   arrival_airport      char(3) comment '到达机场',
   customer_id          int not null comment '客户编号',
   customer_full_name   varchar(50) comment '客户全名',
   gate_number          varchar(10) not null comment '登机口',
   class_level          int comment '舱位等级 1:头等 2:经济',
   seat_no              varchar(5) not null comment '座位号',
   status               int comment '机票状态 1:已出票 2:未出票',
   price                numeric(10,0) not null comment '机票价格',
   primary key (ticket_id)
);

alter table Ticket comment '机票';

/*==============================================================*/
/* Table: opening                                               */
/*==============================================================*/
create table opening
(
   IATA_code            char(3) not null comment '机场编号',
   airline_code         char(2) not null comment '航空公司编号',
   primary key (IATA_code, airline_code)
);

alter table opening comment '机场-开通-航线';

alter table Address add constraint FK_binding2 foreign key (card_code)
      references CreditCard (card_code) on delete restrict on update restrict;

alter table Address add constraint FK_relevance foreign key (customer_id)
      references Customer (customer_id) on delete restrict on update restrict;

alter table Aircraft add constraint FK_belong foreign key (airline_code)
      references Airline (airline_code) on delete restrict on update restrict;

alter table Airport add constraint FK_possess foreign key (city_code)
      references City (city_code) on delete restrict on update restrict;

alter table Booking add constraint FK_create foreign key (customer_id)
      references Customer (customer_id) on delete restrict on update restrict;

alter table Booking add constraint FK_pay2 foreign key (pay_no)
      references Payment (pay_no) on delete restrict on update restrict;

alter table BookingDetail add constraint FK_book foreign key (flight_id)
      references Flight (flight_id) on delete restrict on update restrict;

alter table BookingDetail add constraint FK_detail foreign key (order_id)
      references Booking (order_id) on delete restrict on update restrict;

alter table BookingDetail add constraint "FK_pick passenger" foreign key (contacts_id)
      references Contacts (contacts_id) on delete restrict on update restrict;

alter table Cabin add constraint FK_has foreign key (aircraft_id)
      references Aircraft (aircraft_id) on delete restrict on update restrict;

alter table Contacts add constraint FK_manage foreign key (customer_id)
      references Customer (customer_id) on delete restrict on update restrict;

alter table CreditCard add constraint FK_binding foreign key (address_id)
      references Address (address_id) on delete restrict on update restrict;

alter table CreditCard add constraint FK_owner foreign key (customer_id)
      references Customer (customer_id) on delete restrict on update restrict;

alter table CustomerMileage add constraint "FK_belong to" foreign key (customer_id)
      references Customer (customer_id) on delete restrict on update restrict;

alter table CustomerMileage add constraint FK_owned foreign key (airline_code)
      references Airline (airline_code) on delete restrict on update restrict;

alter table DepartureGate add constraint FK_have foreign key (IATA_code)
      references Airport (IATA_code) on delete restrict on update restrict;

alter table Flight add constraint FK_allocation foreign key (aircraft_id)
      references Aircraft (aircraft_id) on delete restrict on update restrict;

alter table Flight add constraint FK_operate foreign key (airline_code)
      references Airline (airline_code) on delete restrict on update restrict;

alter table MileagePlan add constraint FK_planning foreign key (airline_code)
      references Airline (airline_code) on delete restrict on update restrict;

alter table Payment add constraint FK_pay foreign key (order_id)
      references Booking (order_id) on delete restrict on update restrict;

alter table Payment add constraint "FK_pay for" foreign key (card_code)
      references CreditCard (card_code) on delete restrict on update restrict;

alter table Price add constraint "FK_take on" foreign key (flight_id)
      references Flight (flight_id) on delete restrict on update restrict;

alter table Ticket add constraint FK_generate foreign key (order_id)
      references Booking (order_id) on delete restrict on update restrict;

alter table opening add constraint FK_opening foreign key (IATA_code)
      references Airport (IATA_code) on delete restrict on update restrict;

alter table opening add constraint FK_opening2 foreign key (airline_code)
      references Airline (airline_code) on delete restrict on update restrict;

