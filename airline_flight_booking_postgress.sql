/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     2021/3/20 11:51:52                           */
/*==============================================================*/


drop index binding2_FK;

drop index relevance_FK;

drop index Address_PK;

drop table Address;

drop index belong_FK;

drop index Aircraft_PK;

drop table Aircraft;

drop index Airline_PK;

drop table Airline;

drop index possess_FK;

drop index Airport_PK;

drop table Airport;

drop index create_FK;

drop index pay2_FK;

drop index Booking_PK;

drop table Booking;

drop index book_FK;

drop index "pick passenger_FK";

drop index detail_FK;

drop index BookingDetail_PK;

drop table BookingDetail;

drop index has_FK;

drop index Cabin_PK;

drop table Cabin;

drop index City_PK;

drop table City;

drop index manage_FK;

drop index Contacts_PK;

drop table Contacts;

drop index binding_FK;

drop index owner_FK;

drop index CreditCard_PK;

drop table CreditCard;

drop index Customer_PK;

drop table Customer;

drop index "belong to_FK";

drop index owned_FK;

drop index CustomerMileage_PK;

drop table CustomerMileage;

drop index have_FK;

drop index DepartureGate_PK;

drop table DepartureGate;

drop index allocation_FK;

drop index operate_FK;

drop index Flight_PK;

drop table Flight;

drop index planning_FK;

drop index MileagePlan_PK;

drop table MileagePlan;

drop index "pay for_FK";

drop index pay_FK;

drop index Payment_PK;

drop table Payment;

drop index "take on_FK";

drop index Price_PK;

drop table Price;

drop index generate_FK;

drop index Ticket_PK;

drop table Ticket;

drop index opening2_FK;

drop index opening_FK;

drop index opening_PK;

drop table opening;

/*==============================================================*/
/* Table: Address                                               */
/*==============================================================*/
create table Address (
   addressID            INT4                 not null,
   customerID           INT4                 not null,
   cardID               INT4                 null,
   stateCode            CHAR(2)              not null,
   zipCode              NUMERIC(5)           not null,
   addressName          VARCHAR(255)         null,
   creationDate         DATE                 null,
   constraint PK_ADDRESS primary key (addressID)
);

comment on table Address is
'地址';

comment on column Address.customerID is
'客户编号';

comment on column Address.cardID is
'卡号';

comment on column Address.stateCode is
'州名';

/*==============================================================*/
/* Index: Address_PK                                            */
/*==============================================================*/
create unique index Address_PK on Address (
addressID
);

/*==============================================================*/
/* Index: relevance_FK                                          */
/*==============================================================*/
create  index relevance_FK on Address (
customerID
);

/*==============================================================*/
/* Index: binding2_FK                                           */
/*==============================================================*/
create  index binding2_FK on Address (
cardID
);

/*==============================================================*/
/* Table: Aircraft                                              */
/*==============================================================*/
create table Aircraft (
   aircraftID           INT4                 not null,
   airlineCode          CHAR(2)              not null,
   aircraftType         VARCHAR(30)          null,
   description          TEXT                 null,
   firstClassCapacity   INT4                 null,
   economyClassCapacity INT4                 null,
   constraint PK_AIRCRAFT primary key (aircraftID)
);

comment on table Aircraft is
'飞机';

comment on column Aircraft.description is
'描述';

/*==============================================================*/
/* Index: Aircraft_PK                                           */
/*==============================================================*/
create unique index Aircraft_PK on Aircraft (
aircraftID
);

/*==============================================================*/
/* Index: belong_FK                                             */
/*==============================================================*/
create  index belong_FK on Aircraft (
airlineCode
);

/*==============================================================*/
/* Table: Airline                                               */
/*==============================================================*/
create table Airline (
   airlineCode          CHAR(2)              not null,
   airlineFullName      VARCHAR(255)         null,
   country              VARCHAR(10)          null,
   creationDate         DATE                 null,
   constraint PK_AIRLINE primary key (airlineCode)
);

comment on table Airline is
'航空公司';

comment on column Airline.country is
'国家';

/*==============================================================*/
/* Index: Airline_PK                                            */
/*==============================================================*/
create unique index Airline_PK on Airline (
airlineCode
);

/*==============================================================*/
/* Table: Airport                                               */
/*==============================================================*/
create table Airport (
   IATACode             CHAR(3)              not null,
   cityCode             VARCHAR(10)          not null,
   airportName          VARCHAR(50)          not null,
   status               INT4                 null,
   constraint PK_AIRPORT primary key (IATACode)
);

comment on table Airport is
'机场';

comment on column Airport.cityCode is
'编号';

comment on column Airport.status is
'状态 0:可飞 1停飞';

/*==============================================================*/
/* Index: Airport_PK                                            */
/*==============================================================*/
create unique index Airport_PK on Airport (
IATACode
);

/*==============================================================*/
/* Index: possess_FK                                            */
/*==============================================================*/
create  index possess_FK on Airport (
cityCode
);

/*==============================================================*/
/* Table: Booking                                               */
/*==============================================================*/
create table Booking (
   orderID              VARCHAR(30)          not null,
   customerID           INT4                 not null,
   payNo                VARCHAR(30)          null,
   status               INT4                 not null,
   departureTime        DATE                 null,
   arrivalTime          DATE                 null,
   departureCity        VARCHAR(20)          null,
   arrivalCity          VARCHAR(20)          null,
   creationTime         DATE                 null,
   totalPrice           NUMERIC(10)          not null,
   constraint PK_BOOKING primary key (orderID)
);

comment on table Booking is
'预订';

comment on column Booking.orderID is
'编号';

comment on column Booking.customerID is
'客户编号';

comment on column Booking.payNo is
'编号';

comment on column Booking.status is
'状态 0:可飞 1停飞';

/*==============================================================*/
/* Index: Booking_PK                                            */
/*==============================================================*/
create unique index Booking_PK on Booking (
orderID
);

/*==============================================================*/
/* Index: pay2_FK                                               */
/*==============================================================*/
create  index pay2_FK on Booking (
payNo
);

/*==============================================================*/
/* Index: create_FK                                             */
/*==============================================================*/
create  index create_FK on Booking (
customerID
);

/*==============================================================*/
/* Table: BookingDetail                                         */
/*==============================================================*/
create table BookingDetail (
   detailID             INT4                 not null,
   contactsID           INT4                 not null,
   orderID              VARCHAR(30)          not null,
   flightID             INT4                 not null,
   classLevel           INT4                 null,
   seatNo               VARCHAR(5)           null,
   phone                VARCHAR(15)          null,
   firstName            VARCHAR(30)          null,
   lastName             VARCHAR(30)          null,
   price                NUMERIC(10)          null,
   constraint PK_BOOKINGDETAIL primary key (detailID)
);

comment on table BookingDetail is
'详情';

comment on column BookingDetail.orderID is
'编号';

comment on column BookingDetail.classLevel is
'舱位';

comment on column BookingDetail.phone is
'电话';

comment on column BookingDetail.firstName is
'姓';

comment on column BookingDetail.lastName is
'名';

/*==============================================================*/
/* Index: BookingDetail_PK                                      */
/*==============================================================*/
create unique index BookingDetail_PK on BookingDetail (
detailID
);

/*==============================================================*/
/* Index: detail_FK                                             */
/*==============================================================*/
create  index detail_FK on BookingDetail (
orderID
);

/*==============================================================*/
/* Index: "pick passenger_FK"                                   */
/*==============================================================*/
create  index "pick passenger_FK" on BookingDetail (
contactsID
);

/*==============================================================*/
/* Index: book_FK                                               */
/*==============================================================*/
create  index book_FK on BookingDetail (
flightID
);

/*==============================================================*/
/* Table: Cabin                                                 */
/*==============================================================*/
create table Cabin (
   cabinID              INT4                 not null,
   aircraftID           INT4                 not null,
   number               VARCHAR(5)           null,
   description          TEXT                 null,
   constraint PK_CABIN primary key (cabinID)
);

comment on table Cabin is
'飞机舱位';

comment on column Cabin.description is
'描述';

/*==============================================================*/
/* Index: Cabin_PK                                              */
/*==============================================================*/
create unique index Cabin_PK on Cabin (
cabinID
);

/*==============================================================*/
/* Index: has_FK                                                */
/*==============================================================*/
create  index has_FK on Cabin (
aircraftID
);

/*==============================================================*/
/* Table: City                                                  */
/*==============================================================*/
create table City (
   cityCode             VARCHAR(10)          not null,
   cityName             VARCHAR(20)          not null,
   country              VARCHAR(10)          not null,
   stateCode            CHAR(2)              not null,
   constraint PK_CITY primary key (cityCode)
);

comment on table City is
'城市';

comment on column City.cityCode is
'编号';

comment on column City.cityName is
'名称';

comment on column City.country is
'国家';

comment on column City.stateCode is
'州名';

/*==============================================================*/
/* Index: City_PK                                               */
/*==============================================================*/
create unique index City_PK on City (
cityCode
);

/*==============================================================*/
/* Table: Contacts                                              */
/*==============================================================*/
create table Contacts (
   contactsID           INT4                 not null,
   customerID           INT4                 not null,
   idType               INT4                 null,
   idNumber             VARCHAR(9)           null,
   phone                VARCHAR(15)          null,
   email                VARCHAR(50)          null,
   firstName            VARCHAR(30)          null,
   lastName             VARCHAR(30)          null,
   gender               INT4                 null,
   birth                DATE                 null,
   description          TEXT                 null,
   constraint PK_CONTACTS primary key (contactsID)
);

comment on table Contacts is
'联系人';

comment on column Contacts.customerID is
'客户编号';

comment on column Contacts.phone is
'电话';

comment on column Contacts.email is
'邮箱';

comment on column Contacts.firstName is
'姓';

comment on column Contacts.lastName is
'名';

comment on column Contacts.gender is
'性别';

comment on column Contacts.birth is
'生日';

comment on column Contacts.description is
'描述';

/*==============================================================*/
/* Index: Contacts_PK                                           */
/*==============================================================*/
create unique index Contacts_PK on Contacts (
contactsID
);

/*==============================================================*/
/* Index: manage_FK                                             */
/*==============================================================*/
create  index manage_FK on Contacts (
customerID
);

/*==============================================================*/
/* Table: CreditCard                                            */
/*==============================================================*/
create table CreditCard (
   cardID               INT4                 not null,
   addressID            INT4                 not null,
   customerID           INT4                 not null,
   bank                 VARCHAR(100)         not null,
   effectiveDate        DATE                 null,
   expirationDate       DATE                 null,
   constraint PK_CREDITCARD primary key (cardID)
);

comment on table CreditCard is
'信用卡';

comment on column CreditCard.cardID is
'卡号';

comment on column CreditCard.customerID is
'客户编号';

comment on column CreditCard.bank is
'银行';

comment on column CreditCard.effectiveDate is
'开卡日期';

comment on column CreditCard.expirationDate is
'失效日期';

/*==============================================================*/
/* Index: CreditCard_PK                                         */
/*==============================================================*/
create unique index CreditCard_PK on CreditCard (
cardID
);

/*==============================================================*/
/* Index: owner_FK                                              */
/*==============================================================*/
create  index owner_FK on CreditCard (
customerID
);

/*==============================================================*/
/* Index: binding_FK                                            */
/*==============================================================*/
create  index binding_FK on CreditCard (
addressID
);

/*==============================================================*/
/* Table: Customer                                              */
/*==============================================================*/
create table Customer (
   customerID           INT4                 not null,
   lastName             VARCHAR(30)          not null,
   firstName            VARCHAR(30)          not null,
   ssn                  NUMERIC(9)           null,
   drivingNum           NUMERIC(9)           null,
   passport             VARCHAR(20)          null,
   email                VARCHAR(50)          null,
   phone                VARCHAR(15)          null,
   password             VARCHAR(30)          not null,
   homeAirport          CHAR(3)              null,
   gender               INT4                 not null,
   birth                DATE                 null,
   description          TEXT                 null,
   constraint PK_CUSTOMER primary key (customerID)
);

comment on table Customer is
'客户';

comment on column Customer.customerID is
'客户编号';

comment on column Customer.lastName is
'名';

comment on column Customer.firstName is
'姓';

comment on column Customer.ssn is
'社保号';

comment on column Customer.drivingNum is
'驾驶证号';

comment on column Customer.passport is
'护照';

comment on column Customer.email is
'邮箱';

comment on column Customer.phone is
'电话';

comment on column Customer.password is
'密码';

comment on column Customer.homeAirport is
'机场';

comment on column Customer.gender is
'性别';

comment on column Customer.birth is
'生日';

comment on column Customer.description is
'描述';

/*==============================================================*/
/* Index: Customer_PK                                           */
/*==============================================================*/
create unique index Customer_PK on Customer (
customerID
);

/*==============================================================*/
/* Table: CustomerMileage                                       */
/*==============================================================*/
create table CustomerMileage (
   cmID                 INT4                 not null,
   customerID           INT4                 not null,
   airlineCode          CHAR(2)              not null,
   totalMileage         NUMERIC(10)          null,
   constraint PK_CUSTOMERMILEAGE primary key (cmID)
);

comment on table CustomerMileage is
'客户里程';

comment on column CustomerMileage.customerID is
'客户编号';

/*==============================================================*/
/* Index: CustomerMileage_PK                                    */
/*==============================================================*/
create unique index CustomerMileage_PK on CustomerMileage (
cmID
);

/*==============================================================*/
/* Index: owned_FK                                              */
/*==============================================================*/
create  index owned_FK on CustomerMileage (
airlineCode
);

/*==============================================================*/
/* Index: "belong to_FK"                                        */
/*==============================================================*/
create  index "belong to_FK" on CustomerMileage (
customerID
);

/*==============================================================*/
/* Table: DepartureGate                                         */
/*==============================================================*/
create table DepartureGate (
   gateNumber           VARCHAR(10)          not null,
   IATACode             CHAR(3)              not null,
   description          TEXT                 null,
   constraint PK_DEPARTUREGATE primary key (gateNumber)
);

comment on table DepartureGate is
'登机口';

comment on column DepartureGate.description is
'描述';

/*==============================================================*/
/* Index: DepartureGate_PK                                      */
/*==============================================================*/
create unique index DepartureGate_PK on DepartureGate (
gateNumber
);

/*==============================================================*/
/* Index: have_FK                                               */
/*==============================================================*/
create  index have_FK on DepartureGate (
IATACode
);

/*==============================================================*/
/* Table: Flight                                                */
/*==============================================================*/
create table Flight (
   flightID             INT4                 not null,
   airlineCode          CHAR(2)              not null,
   aircraftID           INT4                 not null,
   flightNumber         VARCHAR(10)          null,
   departureTime        DATE                 null,
   arrivalTime          DATE                 null,
   departureAirpot      CHAR(3)              null,
   arrivalAirport       CHAR(3)              null,
   departureGate        VARCHAR(10)          null,
   firstClassCapacity   INT4                 null,
   economyClassCapacity INT4                 null,
   firstClassPrice      NUMERIC(10)          null,
   economyClassPrice    NUMERIC(10)          null,
   miles                NUMERIC(10)          null,
   constraint PK_FLIGHT primary key (flightID)
);

comment on table Flight is
'航班';

/*==============================================================*/
/* Index: Flight_PK                                             */
/*==============================================================*/
create unique index Flight_PK on Flight (
flightID
);

/*==============================================================*/
/* Index: operate_FK                                            */
/*==============================================================*/
create  index operate_FK on Flight (
airlineCode
);

/*==============================================================*/
/* Index: allocation_FK                                         */
/*==============================================================*/
create  index allocation_FK on Flight (
aircraftID
);

/*==============================================================*/
/* Table: MileagePlan                                           */
/*==============================================================*/
create table MileagePlan (
   mileageNo            INT4                 not null,
   airlineCode          CHAR(2)              not null,
   mileageLevel         INT4                 null,
   miles                NUMERIC(10)          null,
   bonus                TEXT                 null,
   constraint PK_MILEAGEPLAN primary key (mileageNo)
);

comment on table MileagePlan is
'里程计划';

/*==============================================================*/
/* Index: MileagePlan_PK                                        */
/*==============================================================*/
create unique index MileagePlan_PK on MileagePlan (
mileageNo
);

/*==============================================================*/
/* Index: planning_FK                                           */
/*==============================================================*/
create  index planning_FK on MileagePlan (
airlineCode
);

/*==============================================================*/
/* Table: Payment                                               */
/*==============================================================*/
create table Payment (
   payNo                VARCHAR(30)          not null,
   cardID               INT4                 not null,
   orderID              VARCHAR(30)          not null,
   paymentMode          VARCHAR(20)          not null,
   status               INT4                 not null,
   creationTime         DATE                 null,
   paymentTime          DATE                 null,
   constraint PK_PAYMENT primary key (payNo)
);

comment on table Payment is
'支付';

comment on column Payment.payNo is
'编号';

comment on column Payment.cardID is
'卡号';

comment on column Payment.orderID is
'编号';

comment on column Payment.paymentMode is
'方式';

comment on column Payment.status is
'状态 0:可飞 1停飞';

/*==============================================================*/
/* Index: Payment_PK                                            */
/*==============================================================*/
create unique index Payment_PK on Payment (
payNo
);

/*==============================================================*/
/* Index: pay_FK                                                */
/*==============================================================*/
create  index pay_FK on Payment (
orderID
);

/*==============================================================*/
/* Index: "pay for_FK"                                          */
/*==============================================================*/
create  index "pay for_FK" on Payment (
cardID
);

/*==============================================================*/
/* Table: Price                                                 */
/*==============================================================*/
create table Price (
   priceID              INT4                 not null,
   flightID             INT4                 not null,
   classLevel           INT4                 null,
   cost                 NUMERIC(10)          null,
   discount             NUMERIC(2)           null,
   constraint PK_PRICE primary key (priceID)
);

comment on table Price is
'价格';

comment on column Price.priceID is
'编号';

comment on column Price.classLevel is
'舱位';

comment on column Price.cost is
'价格';

comment on column Price.discount is
'折扣';

/*==============================================================*/
/* Index: Price_PK                                              */
/*==============================================================*/
create unique index Price_PK on Price (
priceID
);

/*==============================================================*/
/* Index: "take on_FK"                                          */
/*==============================================================*/
create  index "take on_FK" on Price (
flightID
);

/*==============================================================*/
/* Table: Ticket                                                */
/*==============================================================*/
create table Ticket (
   ticketID             INT4                 not null,
   orderID              VARCHAR(30)          not null,
   airlineCode          CHAR(2)              null,
   airlineFullName      VARCHAR(255)         null,
   flightID             INT4                 null,
   flightNumber         VARCHAR(10)          null,
   departureTime        DATE                 null,
   arrivalTime          DATE                 null,
   departureAirport     CHAR(3)              null,
   arrivalAirport       CHAR(3)              null,
   customerID           INT4                 null,
   customerFullName     VARCHAR(50)          null,
   gateNumber           VARCHAR(10)          null,
   classLevel           INT4                 null,
   seatNo               VARCHAR(5)           null,
   status               INT4                 null,
   price                NUMERIC(10)          null,
   constraint PK_TICKET primary key (ticketID)
);

comment on table Ticket is
'机票';

comment on column Ticket.orderID is
'编号';

comment on column Ticket.classLevel is
'1:头等 2:经济';

comment on column Ticket.status is
'1:已出票 2:未出票';

/*==============================================================*/
/* Index: Ticket_PK                                             */
/*==============================================================*/
create unique index Ticket_PK on Ticket (
ticketID
);

/*==============================================================*/
/* Index: generate_FK                                           */
/*==============================================================*/
create  index generate_FK on Ticket (
orderID
);

/*==============================================================*/
/* Table: opening                                               */
/*==============================================================*/
create table opening (
   IATACode             CHAR(3)              not null,
   airlineCode          CHAR(2)              not null,
   constraint PK_OPENING primary key (IATACode, airlineCode)
);

comment on table opening is
'机场-开通-航线';

/*==============================================================*/
/* Index: opening_PK                                            */
/*==============================================================*/
create unique index opening_PK on opening (
IATACode,
airlineCode
);

/*==============================================================*/
/* Index: opening_FK                                            */
/*==============================================================*/
create  index opening_FK on opening (
IATACode
);

/*==============================================================*/
/* Index: opening2_FK                                           */
/*==============================================================*/
create  index opening2_FK on opening (
airlineCode
);

alter table Address
   add constraint FK_ADDRESS_BINDING2_CREDITCA foreign key (cardID)
      references CreditCard (cardID)
      on delete restrict on update restrict;

alter table Address
   add constraint FK_ADDRESS_RELEVANCE_CUSTOMER foreign key (customerID)
      references Customer (customerID)
      on delete restrict on update restrict;

alter table Aircraft
   add constraint FK_AIRCRAFT_BELONG_AIRLINE foreign key (airlineCode)
      references Airline (airlineCode)
      on delete restrict on update restrict;

alter table Airport
   add constraint FK_AIRPORT_POSSESS_CITY foreign key (cityCode)
      references City (cityCode)
      on delete restrict on update restrict;

alter table Booking
   add constraint FK_BOOKING_CREATE_CUSTOMER foreign key (customerID)
      references Customer (customerID)
      on delete restrict on update restrict;

alter table Booking
   add constraint FK_BOOKING_PAY2_PAYMENT foreign key (payNo)
      references Payment (payNo)
      on delete restrict on update restrict;

alter table BookingDetail
   add constraint FK_BOOKINGD_BOOK_FLIGHT foreign key (flightID)
      references Flight (flightID)
      on delete restrict on update restrict;

alter table BookingDetail
   add constraint FK_BOOKINGD_DETAIL_BOOKING foreign key (orderID)
      references Booking (orderID)
      on delete restrict on update restrict;

alter table BookingDetail
   add constraint "FK_BOOKINGD_PICK PASS_CONTACTS" foreign key (contactsID)
      references Contacts (contactsID)
      on delete restrict on update restrict;

alter table Cabin
   add constraint FK_CABIN_HAS_AIRCRAFT foreign key (aircraftID)
      references Aircraft (aircraftID)
      on delete restrict on update restrict;

alter table Contacts
   add constraint FK_CONTACTS_MANAGE_CUSTOMER foreign key (customerID)
      references Customer (customerID)
      on delete restrict on update restrict;

alter table CreditCard
   add constraint FK_CREDITCA_BINDING_ADDRESS foreign key (addressID)
      references Address (addressID)
      on delete restrict on update restrict;

alter table CreditCard
   add constraint FK_CREDITCA_OWNER_CUSTOMER foreign key (customerID)
      references Customer (customerID)
      on delete restrict on update restrict;

alter table CustomerMileage
   add constraint "FK_CUSTOMER_BELONG TO_CUSTOMER" foreign key (customerID)
      references Customer (customerID)
      on delete restrict on update restrict;

alter table CustomerMileage
   add constraint FK_CUSTOMER_OWNED_AIRLINE foreign key (airlineCode)
      references Airline (airlineCode)
      on delete restrict on update restrict;

alter table DepartureGate
   add constraint FK_DEPARTUR_HAVE_AIRPORT foreign key (IATACode)
      references Airport (IATACode)
      on delete restrict on update restrict;

alter table Flight
   add constraint FK_FLIGHT_ALLOCATIO_AIRCRAFT foreign key (aircraftID)
      references Aircraft (aircraftID)
      on delete restrict on update restrict;

alter table Flight
   add constraint FK_FLIGHT_OPERATE_AIRLINE foreign key (airlineCode)
      references Airline (airlineCode)
      on delete restrict on update restrict;

alter table MileagePlan
   add constraint FK_MILEAGEP_PLANNING_AIRLINE foreign key (airlineCode)
      references Airline (airlineCode)
      on delete restrict on update restrict;

alter table Payment
   add constraint FK_PAYMENT_PAY_BOOKING foreign key (orderID)
      references Booking (orderID)
      on delete restrict on update restrict;

alter table Payment
   add constraint "FK_PAYMENT_PAY FOR_CREDITCA" foreign key (cardID)
      references CreditCard (cardID)
      on delete restrict on update restrict;

alter table Price
   add constraint "FK_PRICE_TAKE ON_FLIGHT" foreign key (flightID)
      references Flight (flightID)
      on delete restrict on update restrict;

alter table Ticket
   add constraint FK_TICKET_GENERATE_BOOKING foreign key (orderID)
      references Booking (orderID)
      on delete restrict on update restrict;

alter table opening
   add constraint FK_OPENING_OPENING_AIRPORT foreign key (IATACode)
      references Airport (IATACode)
      on delete restrict on update restrict;

alter table opening
   add constraint FK_OPENING_OPENING2_AIRLINE foreign key (airlineCode)
      references Airline (airlineCode)
      on delete restrict on update restrict;

