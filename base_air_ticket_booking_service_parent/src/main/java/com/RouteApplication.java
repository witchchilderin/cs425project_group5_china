package com;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

/**
 * @Auther: WeiWu
 * @Date: 2021/5/11 15:22
 * @Description:程序入口
 */
@Configuration
@SpringBootApplication(scanBasePackages = {"com.route.*"})
@MapperScan({"com.route.dao.mapper"})
@EnableDiscoveryClient
public class RouteApplication {

    private static final Logger log = LoggerFactory.getLogger(RouteApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(RouteApplication.class, args);
        log.error("====启动成功====");
    }

}
