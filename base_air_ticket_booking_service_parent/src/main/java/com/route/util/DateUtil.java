package com.route.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static final String ALL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Date getNowDate() {
        SimpleDateFormat formatter = new SimpleDateFormat(ALL_DATE_FORMAT);
        String allDateTime = formatter.format(new Date());
        Date nowDate;
        try {
            nowDate = formatter.parse(allDateTime);
        } catch (ParseException e) {
            nowDate = new Date();
        }
        return nowDate;
    }

    public static String getNowDateFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat(ALL_DATE_FORMAT);
        return sdf.format(getNowDate());
    }

    public static Date getDateFormat(String dateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat(ALL_DATE_FORMAT);
        try {
            return sdf.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getMonthFirst(String date) throws ParseException {
        Calendar c = Calendar.getInstance();
        c.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天
        String first = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return first;
    }

    public static String getMonthLast(String date) throws ParseException {
        Calendar c = Calendar.getInstance();
        c.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
        c.add(Calendar.MONTH, 1);
        c.set(Calendar.DAY_OF_MONTH, 0);
        String first = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return first;
    }

}
