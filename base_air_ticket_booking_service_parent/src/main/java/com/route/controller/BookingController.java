package com.route.controller;

import com.route.dao.po.Booking;
import com.route.entity.request.QueryBookingRequestEntity;
import com.route.entity.response.ResponseEntity;
import com.route.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Date: 2021/5/11 10:47
 * @Description: Booking order API Class
 */
@RestController
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    private BookingService bookingService;

    /**
     * 预订订单信息条件查询通用方法
     * 功能描述: 预订订单信息条件查询通用方法
     * @param: String orderId  订单编号
     * @param: Integer customerId  客户编号
     * @param: Integer status  状态  0:可飞 1停飞
     * @param: String departureTime    起飞时间
     * @param: String arrivalTime  到达时间
     * @param: String departureCity    起飞城市
     * @param: String arrivalCity  到达城市
     * @param: String creationTime 创建日期
     * @param: long totalPriceStart 起始总价格
     * @param: long totalPriceEnd 结束总价格
     * @return: 预订数据实体列表
     * @date: 2021/5/11 11:08
     */
    @RequestMapping(value = "/querybookinglistbypage",method = RequestMethod.POST)
    public ResponseEntity queryBookingListByPage(@RequestParam(value = "customerId") Integer customerId,
                                                 QueryBookingRequestEntity requestEntity){
        //声明并初始化结果集
        Map<String,Object> resultMap = new HashMap<String,Object>();
        resultMap.put("total",0);
        resultMap.put("dataList",new ArrayList<Booking>(0));
        //查询数据统计条数
        long total = bookingService.countBookingTotal(customerId,requestEntity);
        resultMap.put("total",total);
        if(total > 0){
            //查询数据
            List<Booking> list = bookingService.queryBookingListByPage(customerId,requestEntity);
            resultMap.put("dataList",list);
        }
        //创建返回实体
        return ResponseEntity.getSuccessByEntity("success",resultMap);
    }

}
