package com.route.controller;

import com.route.entity.request.QueryFlightListRequestEntity;
import com.route.entity.response.ResponseEntity;
import com.route.service.FlightService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Date: 2021/5/14 20:27
 * @Description: Flight API Class
 */
@RestController
@RequestMapping("/flight")
public class FlightController {

    @Resource
    private FlightService flightService;

    /**
     * @Description: Query flight list data
     * @param: departureAirpot
     * @param: arrivalAirport
     * @param: pageNum
     * @param: pageSize
     * @return: ResponseEntity
     * @date: 2021/5/14 21:09
     */
    @RequestMapping(value = "/queryflightlistbypage",method = RequestMethod.POST)
    public ResponseEntity queryFlightListByPage(QueryFlightListRequestEntity requestEntity){
        return flightService.queryFlightListByPage(requestEntity);
    }

}
