package com.route.controller;

import com.route.entity.response.ResponseEntity;
import com.route.service.CreditCardService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Date: 2021/5/13 11:09
 * @Description: Creditcard API Class
 */
@RestController
@RequestMapping("/creditcard")
public class CreditCardController {

    @Resource
    private CreditCardService creditCardService;

    /**
     * Query creditcard list by customerId API
     * 功能描述: Query creditcard list by customerId API
     * @param: customerId
     * @return: ResponseEntity
     * @date: 2021/5/13 11:17
     */
    @RequestMapping(value = "/querycreditcardlistbycustomerid",method = RequestMethod.POST)
    public ResponseEntity queryCreditCardListByCustomerId(@RequestParam(value = "customerId") Integer customerId){
        return creditCardService.queryCreditCardListByCustomerId(customerId);
    }

}
