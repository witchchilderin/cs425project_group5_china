package com.route.controller;

import com.route.entity.request.RegisterInfoRequestEntity;
import com.route.entity.request.UpdateCustomerInfoRequestEntity;
import com.route.entity.response.ResponseEntity;
import com.route.service.CustomerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
 * @Auther: WeiWU
 * @Date: 2021/5/12 11:07
 * @Description: Customer API Class
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Resource
    private CustomerService customerService;

    /**
     * @Description: Register API
     * @param: String lastName
     * @param: String firstName
     * @param: String email
     * @param: String password
     * @return: ResponseEntity
     * @date: 2021/5/12 11:22
     */
    @RequestMapping(value = "/register",method = RequestMethod.POST)
    public ResponseEntity register(RegisterInfoRequestEntity requestEntity){
        ResponseEntity responseEntity = customerService.register(requestEntity);
        return responseEntity;
    }

    @RequestMapping(value = "/querycustomerinfobycustomerid",method = RequestMethod.POST)
    public ResponseEntity queryCustomerInfoByCustomerId(@RequestParam(value = "customerId") Integer customerId){
        return customerService.queryCustomerInfoByCustomerId(customerId);
    }

    @RequestMapping(value = "/updatecustomerinfoycustomerid",method = RequestMethod.POST)
    public ResponseEntity updateCustomerInfoByCustomerId(UpdateCustomerInfoRequestEntity requestEntity){
        return customerService.updateCustomerInfoByCustomerId(requestEntity);
    }

}
