package com.route.controller;

import com.route.entity.response.ResponseEntity;
import com.route.service.PayService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

/**
 * @Auther: wuwei
 * @Date: 2021/5/13 10:33
 * @Description: Pay API Class
 */
@RestController
@RequestMapping("/pay")
public class PayController {

    @Resource
    private PayService payService;

    /**
     * @Description: Pay API Class
     * @param: customerId
     * @param: orderId
     * @return: ResponseEntity
     * @date: 2021/5/14 12:34
     */
    @RequestMapping(value = "/dopay",method = RequestMethod.POST)
    public ResponseEntity doPay(@RequestParam(value = "customerId") Integer customerId,
                                @RequestParam(value = "orderId") String orderId){
        return payService.doPay(customerId,orderId);
    }

}
