package com.route.dao.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Address implements Serializable {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Address.address_id
     *
     * @mbg.generated
     */
    private Integer addressId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Address.customer_id
     *
     * @mbg.generated
     */
    private Integer customerId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Address.card_code
     *
     * @mbg.generated
     */
    private String cardCode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Address.state_code
     *
     * @mbg.generated
     */
    private String stateCode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Address.zip_code
     *
     * @mbg.generated
     */
    private BigDecimal zipCode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Address.address_name
     *
     * @mbg.generated
     */
    private String addressName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Address.creation_date
     *
     * @mbg.generated
     */
    private Date creationDate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table Address
     *
     * @mbg.generated
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Address.address_id
     *
     * @return the value of Address.address_id
     *
     * @mbg.generated
     */
    public Integer getAddressId() {
        return addressId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Address.address_id
     *
     * @param addressId the value for Address.address_id
     *
     * @mbg.generated
     */
    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Address.customer_id
     *
     * @return the value of Address.customer_id
     *
     * @mbg.generated
     */
    public Integer getCustomerId() {
        return customerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Address.customer_id
     *
     * @param customerId the value for Address.customer_id
     *
     * @mbg.generated
     */
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Address.card_code
     *
     * @return the value of Address.card_code
     *
     * @mbg.generated
     */
    public String getCardCode() {
        return cardCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Address.card_code
     *
     * @param cardCode the value for Address.card_code
     *
     * @mbg.generated
     */
    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Address.state_code
     *
     * @return the value of Address.state_code
     *
     * @mbg.generated
     */
    public String getStateCode() {
        return stateCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Address.state_code
     *
     * @param stateCode the value for Address.state_code
     *
     * @mbg.generated
     */
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Address.zip_code
     *
     * @return the value of Address.zip_code
     *
     * @mbg.generated
     */
    public BigDecimal getZipCode() {
        return zipCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Address.zip_code
     *
     * @param zipCode the value for Address.zip_code
     *
     * @mbg.generated
     */
    public void setZipCode(BigDecimal zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Address.address_name
     *
     * @return the value of Address.address_name
     *
     * @mbg.generated
     */
    public String getAddressName() {
        return addressName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Address.address_name
     *
     * @param addressName the value for Address.address_name
     *
     * @mbg.generated
     */
    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Address.creation_date
     *
     * @return the value of Address.creation_date
     *
     * @mbg.generated
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Address.creation_date
     *
     * @param creationDate the value for Address.creation_date
     *
     * @mbg.generated
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}