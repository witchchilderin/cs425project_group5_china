package com.route.dao.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Flight implements Serializable {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.flight_id
     *
     * @mbg.generated
     */
    private Integer flightId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.airline_code
     *
     * @mbg.generated
     */
    private String airlineCode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.aircraft_id
     *
     * @mbg.generated
     */
    private Integer aircraftId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.flight_number
     *
     * @mbg.generated
     */
    private String flightNumber;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.departure_time
     *
     * @mbg.generated
     */
    private Date departureTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.arrival_time
     *
     * @mbg.generated
     */
    private Date arrivalTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.departure_airpot
     *
     * @mbg.generated
     */
    private String departureAirpot;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.arrival_airport
     *
     * @mbg.generated
     */
    private String arrivalAirport;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.departure_gate
     *
     * @mbg.generated
     */
    private String departureGate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.first_class_capacity
     *
     * @mbg.generated
     */
    private Integer firstClassCapacity;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.economy_class_capacity
     *
     * @mbg.generated
     */
    private Integer economyClassCapacity;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.first_class_price
     *
     * @mbg.generated
     */
    private BigDecimal firstClassPrice;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.economy_class_price
     *
     * @mbg.generated
     */
    private BigDecimal economyClassPrice;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Flight.miles
     *
     * @mbg.generated
     */
    private BigDecimal miles;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table Flight
     *
     * @mbg.generated
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.flight_id
     *
     * @return the value of Flight.flight_id
     *
     * @mbg.generated
     */
    public Integer getFlightId() {
        return flightId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.flight_id
     *
     * @param flightId the value for Flight.flight_id
     *
     * @mbg.generated
     */
    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.airline_code
     *
     * @return the value of Flight.airline_code
     *
     * @mbg.generated
     */
    public String getAirlineCode() {
        return airlineCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.airline_code
     *
     * @param airlineCode the value for Flight.airline_code
     *
     * @mbg.generated
     */
    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.aircraft_id
     *
     * @return the value of Flight.aircraft_id
     *
     * @mbg.generated
     */
    public Integer getAircraftId() {
        return aircraftId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.aircraft_id
     *
     * @param aircraftId the value for Flight.aircraft_id
     *
     * @mbg.generated
     */
    public void setAircraftId(Integer aircraftId) {
        this.aircraftId = aircraftId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.flight_number
     *
     * @return the value of Flight.flight_number
     *
     * @mbg.generated
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.flight_number
     *
     * @param flightNumber the value for Flight.flight_number
     *
     * @mbg.generated
     */
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.departure_time
     *
     * @return the value of Flight.departure_time
     *
     * @mbg.generated
     */
    public Date getDepartureTime() {
        return departureTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.departure_time
     *
     * @param departureTime the value for Flight.departure_time
     *
     * @mbg.generated
     */
    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.arrival_time
     *
     * @return the value of Flight.arrival_time
     *
     * @mbg.generated
     */
    public Date getArrivalTime() {
        return arrivalTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.arrival_time
     *
     * @param arrivalTime the value for Flight.arrival_time
     *
     * @mbg.generated
     */
    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.departure_airpot
     *
     * @return the value of Flight.departure_airpot
     *
     * @mbg.generated
     */
    public String getDepartureAirpot() {
        return departureAirpot;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.departure_airpot
     *
     * @param departureAirpot the value for Flight.departure_airpot
     *
     * @mbg.generated
     */
    public void setDepartureAirpot(String departureAirpot) {
        this.departureAirpot = departureAirpot;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.arrival_airport
     *
     * @return the value of Flight.arrival_airport
     *
     * @mbg.generated
     */
    public String getArrivalAirport() {
        return arrivalAirport;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.arrival_airport
     *
     * @param arrivalAirport the value for Flight.arrival_airport
     *
     * @mbg.generated
     */
    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.departure_gate
     *
     * @return the value of Flight.departure_gate
     *
     * @mbg.generated
     */
    public String getDepartureGate() {
        return departureGate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.departure_gate
     *
     * @param departureGate the value for Flight.departure_gate
     *
     * @mbg.generated
     */
    public void setDepartureGate(String departureGate) {
        this.departureGate = departureGate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.first_class_capacity
     *
     * @return the value of Flight.first_class_capacity
     *
     * @mbg.generated
     */
    public Integer getFirstClassCapacity() {
        return firstClassCapacity;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.first_class_capacity
     *
     * @param firstClassCapacity the value for Flight.first_class_capacity
     *
     * @mbg.generated
     */
    public void setFirstClassCapacity(Integer firstClassCapacity) {
        this.firstClassCapacity = firstClassCapacity;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.economy_class_capacity
     *
     * @return the value of Flight.economy_class_capacity
     *
     * @mbg.generated
     */
    public Integer getEconomyClassCapacity() {
        return economyClassCapacity;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.economy_class_capacity
     *
     * @param economyClassCapacity the value for Flight.economy_class_capacity
     *
     * @mbg.generated
     */
    public void setEconomyClassCapacity(Integer economyClassCapacity) {
        this.economyClassCapacity = economyClassCapacity;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.first_class_price
     *
     * @return the value of Flight.first_class_price
     *
     * @mbg.generated
     */
    public BigDecimal getFirstClassPrice() {
        return firstClassPrice;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.first_class_price
     *
     * @param firstClassPrice the value for Flight.first_class_price
     *
     * @mbg.generated
     */
    public void setFirstClassPrice(BigDecimal firstClassPrice) {
        this.firstClassPrice = firstClassPrice;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.economy_class_price
     *
     * @return the value of Flight.economy_class_price
     *
     * @mbg.generated
     */
    public BigDecimal getEconomyClassPrice() {
        return economyClassPrice;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.economy_class_price
     *
     * @param economyClassPrice the value for Flight.economy_class_price
     *
     * @mbg.generated
     */
    public void setEconomyClassPrice(BigDecimal economyClassPrice) {
        this.economyClassPrice = economyClassPrice;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Flight.miles
     *
     * @return the value of Flight.miles
     *
     * @mbg.generated
     */
    public BigDecimal getMiles() {
        return miles;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Flight.miles
     *
     * @param miles the value for Flight.miles
     *
     * @mbg.generated
     */
    public void setMiles(BigDecimal miles) {
        this.miles = miles;
    }
}