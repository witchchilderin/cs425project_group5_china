package com.route.dao.po;

import java.io.Serializable;

public class Airport implements Serializable {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Airport.IATA_code
     *
     * @mbg.generated
     */
    private String iataCode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Airport.city_code
     *
     * @mbg.generated
     */
    private String cityCode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Airport.airport_name
     *
     * @mbg.generated
     */
    private String airportName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Airport.status
     *
     * @mbg.generated
     */
    private Integer status;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table Airport
     *
     * @mbg.generated
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Airport.IATA_code
     *
     * @return the value of Airport.IATA_code
     *
     * @mbg.generated
     */
    public String getIataCode() {
        return iataCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Airport.IATA_code
     *
     * @param iataCode the value for Airport.IATA_code
     *
     * @mbg.generated
     */
    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Airport.city_code
     *
     * @return the value of Airport.city_code
     *
     * @mbg.generated
     */
    public String getCityCode() {
        return cityCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Airport.city_code
     *
     * @param cityCode the value for Airport.city_code
     *
     * @mbg.generated
     */
    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Airport.airport_name
     *
     * @return the value of Airport.airport_name
     *
     * @mbg.generated
     */
    public String getAirportName() {
        return airportName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Airport.airport_name
     *
     * @param airportName the value for Airport.airport_name
     *
     * @mbg.generated
     */
    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Airport.status
     *
     * @return the value of Airport.status
     *
     * @mbg.generated
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Airport.status
     *
     * @param status the value for Airport.status
     *
     * @mbg.generated
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
}