package com.route.dao.po;

import java.util.ArrayList;
import java.util.List;

public class OpeningExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table opening
     *
     * @mbg.generated
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table opening
     *
     * @mbg.generated
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table opening
     *
     * @mbg.generated
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table opening
     *
     * @mbg.generated
     */
    public OpeningExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table opening
     *
     * @mbg.generated
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table opening
     *
     * @mbg.generated
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table opening
     *
     * @mbg.generated
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table opening
     *
     * @mbg.generated
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table opening
     *
     * @mbg.generated
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table opening
     *
     * @mbg.generated
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table opening
     *
     * @mbg.generated
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table opening
     *
     * @mbg.generated
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table opening
     *
     * @mbg.generated
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table opening
     *
     * @mbg.generated
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table opening
     *
     * @mbg.generated
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIataCodeIsNull() {
            addCriterion("IATA_code is null");
            return (Criteria) this;
        }

        public Criteria andIataCodeIsNotNull() {
            addCriterion("IATA_code is not null");
            return (Criteria) this;
        }

        public Criteria andIataCodeEqualTo(String value) {
            addCriterion("IATA_code =", value, "iataCode");
            return (Criteria) this;
        }

        public Criteria andIataCodeNotEqualTo(String value) {
            addCriterion("IATA_code <>", value, "iataCode");
            return (Criteria) this;
        }

        public Criteria andIataCodeGreaterThan(String value) {
            addCriterion("IATA_code >", value, "iataCode");
            return (Criteria) this;
        }

        public Criteria andIataCodeGreaterThanOrEqualTo(String value) {
            addCriterion("IATA_code >=", value, "iataCode");
            return (Criteria) this;
        }

        public Criteria andIataCodeLessThan(String value) {
            addCriterion("IATA_code <", value, "iataCode");
            return (Criteria) this;
        }

        public Criteria andIataCodeLessThanOrEqualTo(String value) {
            addCriterion("IATA_code <=", value, "iataCode");
            return (Criteria) this;
        }

        public Criteria andIataCodeLike(String value) {
            addCriterion("IATA_code like", value, "iataCode");
            return (Criteria) this;
        }

        public Criteria andIataCodeNotLike(String value) {
            addCriterion("IATA_code not like", value, "iataCode");
            return (Criteria) this;
        }

        public Criteria andIataCodeIn(List<String> values) {
            addCriterion("IATA_code in", values, "iataCode");
            return (Criteria) this;
        }

        public Criteria andIataCodeNotIn(List<String> values) {
            addCriterion("IATA_code not in", values, "iataCode");
            return (Criteria) this;
        }

        public Criteria andIataCodeBetween(String value1, String value2) {
            addCriterion("IATA_code between", value1, value2, "iataCode");
            return (Criteria) this;
        }

        public Criteria andIataCodeNotBetween(String value1, String value2) {
            addCriterion("IATA_code not between", value1, value2, "iataCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeIsNull() {
            addCriterion("airline_code is null");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeIsNotNull() {
            addCriterion("airline_code is not null");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeEqualTo(String value) {
            addCriterion("airline_code =", value, "airlineCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeNotEqualTo(String value) {
            addCriterion("airline_code <>", value, "airlineCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeGreaterThan(String value) {
            addCriterion("airline_code >", value, "airlineCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeGreaterThanOrEqualTo(String value) {
            addCriterion("airline_code >=", value, "airlineCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeLessThan(String value) {
            addCriterion("airline_code <", value, "airlineCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeLessThanOrEqualTo(String value) {
            addCriterion("airline_code <=", value, "airlineCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeLike(String value) {
            addCriterion("airline_code like", value, "airlineCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeNotLike(String value) {
            addCriterion("airline_code not like", value, "airlineCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeIn(List<String> values) {
            addCriterion("airline_code in", values, "airlineCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeNotIn(List<String> values) {
            addCriterion("airline_code not in", values, "airlineCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeBetween(String value1, String value2) {
            addCriterion("airline_code between", value1, value2, "airlineCode");
            return (Criteria) this;
        }

        public Criteria andAirlineCodeNotBetween(String value1, String value2) {
            addCriterion("airline_code not between", value1, value2, "airlineCode");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table opening
     *
     * @mbg.generated do_not_delete_during_merge
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table opening
     *
     * @mbg.generated
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}