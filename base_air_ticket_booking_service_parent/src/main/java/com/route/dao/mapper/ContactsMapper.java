package com.route.dao.mapper;

import com.route.dao.po.Contacts;
import com.route.dao.po.ContactsExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface ContactsMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @SelectProvider(type=ContactsSqlProvider.class, method="countByExample")
    long countByExample(ContactsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @DeleteProvider(type=ContactsSqlProvider.class, method="deleteByExample")
    int deleteByExample(ContactsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @Delete({
        "delete from Contacts",
        "where contacts_id = #{contactsId,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer contactsId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @Insert({
        "insert into Contacts (contacts_id, customer_id, ",
        "id_type, id_number, ",
        "phone, email, first_name, ",
        "last_name, gender, ",
        "birth, description)",
        "values (#{contactsId,jdbcType=INTEGER}, #{customerId,jdbcType=INTEGER}, ",
        "#{idType,jdbcType=INTEGER}, #{idNumber,jdbcType=VARCHAR}, ",
        "#{phone,jdbcType=VARCHAR}, #{email,jdbcType=VARCHAR}, #{firstName,jdbcType=VARCHAR}, ",
        "#{lastName,jdbcType=VARCHAR}, #{gender,jdbcType=INTEGER}, ",
        "#{birth,jdbcType=DATE}, #{description,jdbcType=LONGVARCHAR})"
    })
    int insert(Contacts record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @InsertProvider(type=ContactsSqlProvider.class, method="insertSelective")
    int insertSelective(Contacts record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @SelectProvider(type=ContactsSqlProvider.class, method="selectByExampleWithBLOBs")
    @Results({
        @Result(column="contacts_id", property="contactsId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="customer_id", property="customerId", jdbcType=JdbcType.INTEGER),
        @Result(column="id_type", property="idType", jdbcType=JdbcType.INTEGER),
        @Result(column="id_number", property="idNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="phone", property="phone", jdbcType=JdbcType.VARCHAR),
        @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
        @Result(column="first_name", property="firstName", jdbcType=JdbcType.VARCHAR),
        @Result(column="last_name", property="lastName", jdbcType=JdbcType.VARCHAR),
        @Result(column="gender", property="gender", jdbcType=JdbcType.INTEGER),
        @Result(column="birth", property="birth", jdbcType=JdbcType.DATE),
        @Result(column="description", property="description", jdbcType=JdbcType.LONGVARCHAR)
    })
    List<Contacts> selectByExampleWithBLOBs(ContactsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @SelectProvider(type=ContactsSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="contacts_id", property="contactsId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="customer_id", property="customerId", jdbcType=JdbcType.INTEGER),
        @Result(column="id_type", property="idType", jdbcType=JdbcType.INTEGER),
        @Result(column="id_number", property="idNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="phone", property="phone", jdbcType=JdbcType.VARCHAR),
        @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
        @Result(column="first_name", property="firstName", jdbcType=JdbcType.VARCHAR),
        @Result(column="last_name", property="lastName", jdbcType=JdbcType.VARCHAR),
        @Result(column="gender", property="gender", jdbcType=JdbcType.INTEGER),
        @Result(column="birth", property="birth", jdbcType=JdbcType.DATE)
    })
    List<Contacts> selectByExample(ContactsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @Select({
        "select",
        "contacts_id, customer_id, id_type, id_number, phone, email, first_name, last_name, ",
        "gender, birth, description",
        "from Contacts",
        "where contacts_id = #{contactsId,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="contacts_id", property="contactsId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="customer_id", property="customerId", jdbcType=JdbcType.INTEGER),
        @Result(column="id_type", property="idType", jdbcType=JdbcType.INTEGER),
        @Result(column="id_number", property="idNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="phone", property="phone", jdbcType=JdbcType.VARCHAR),
        @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
        @Result(column="first_name", property="firstName", jdbcType=JdbcType.VARCHAR),
        @Result(column="last_name", property="lastName", jdbcType=JdbcType.VARCHAR),
        @Result(column="gender", property="gender", jdbcType=JdbcType.INTEGER),
        @Result(column="birth", property="birth", jdbcType=JdbcType.DATE),
        @Result(column="description", property="description", jdbcType=JdbcType.LONGVARCHAR)
    })
    Contacts selectByPrimaryKey(Integer contactsId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @UpdateProvider(type=ContactsSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Contacts record, @Param("example") ContactsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @UpdateProvider(type=ContactsSqlProvider.class, method="updateByExampleWithBLOBs")
    int updateByExampleWithBLOBs(@Param("record") Contacts record, @Param("example") ContactsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @UpdateProvider(type=ContactsSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Contacts record, @Param("example") ContactsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @UpdateProvider(type=ContactsSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Contacts record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @Update({
        "update Contacts",
        "set customer_id = #{customerId,jdbcType=INTEGER},",
          "id_type = #{idType,jdbcType=INTEGER},",
          "id_number = #{idNumber,jdbcType=VARCHAR},",
          "phone = #{phone,jdbcType=VARCHAR},",
          "email = #{email,jdbcType=VARCHAR},",
          "first_name = #{firstName,jdbcType=VARCHAR},",
          "last_name = #{lastName,jdbcType=VARCHAR},",
          "gender = #{gender,jdbcType=INTEGER},",
          "birth = #{birth,jdbcType=DATE},",
          "description = #{description,jdbcType=LONGVARCHAR}",
        "where contacts_id = #{contactsId,jdbcType=INTEGER}"
    })
    int updateByPrimaryKeyWithBLOBs(Contacts record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Contacts
     *
     * @mbg.generated
     */
    @Update({
        "update Contacts",
        "set customer_id = #{customerId,jdbcType=INTEGER},",
          "id_type = #{idType,jdbcType=INTEGER},",
          "id_number = #{idNumber,jdbcType=VARCHAR},",
          "phone = #{phone,jdbcType=VARCHAR},",
          "email = #{email,jdbcType=VARCHAR},",
          "first_name = #{firstName,jdbcType=VARCHAR},",
          "last_name = #{lastName,jdbcType=VARCHAR},",
          "gender = #{gender,jdbcType=INTEGER},",
          "birth = #{birth,jdbcType=DATE}",
        "where contacts_id = #{contactsId,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Contacts record);
}