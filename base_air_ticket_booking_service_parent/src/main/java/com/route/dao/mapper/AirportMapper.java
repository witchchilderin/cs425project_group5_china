package com.route.dao.mapper;

import com.route.dao.po.Airport;
import com.route.dao.po.AirportExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface AirportMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Airport
     *
     * @mbg.generated
     */
    @SelectProvider(type=AirportSqlProvider.class, method="countByExample")
    long countByExample(AirportExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Airport
     *
     * @mbg.generated
     */
    @DeleteProvider(type=AirportSqlProvider.class, method="deleteByExample")
    int deleteByExample(AirportExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Airport
     *
     * @mbg.generated
     */
    @Delete({
        "delete from Airport",
        "where IATA_code = #{iataCode,jdbcType=CHAR}"
    })
    int deleteByPrimaryKey(String iataCode);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Airport
     *
     * @mbg.generated
     */
    @Insert({
        "insert into Airport (IATA_code, city_code, ",
        "airport_name, status)",
        "values (#{iataCode,jdbcType=CHAR}, #{cityCode,jdbcType=VARCHAR}, ",
        "#{airportName,jdbcType=VARCHAR}, #{status,jdbcType=INTEGER})"
    })
    int insert(Airport record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Airport
     *
     * @mbg.generated
     */
    @InsertProvider(type=AirportSqlProvider.class, method="insertSelective")
    int insertSelective(Airport record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Airport
     *
     * @mbg.generated
     */
    @SelectProvider(type=AirportSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="IATA_code", property="iataCode", jdbcType=JdbcType.CHAR, id=true),
        @Result(column="city_code", property="cityCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="airport_name", property="airportName", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.INTEGER)
    })
    List<Airport> selectByExample(AirportExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Airport
     *
     * @mbg.generated
     */
    @Select({
        "select",
        "IATA_code, city_code, airport_name, status",
        "from Airport",
        "where IATA_code = #{iataCode,jdbcType=CHAR}"
    })
    @Results({
        @Result(column="IATA_code", property="iataCode", jdbcType=JdbcType.CHAR, id=true),
        @Result(column="city_code", property="cityCode", jdbcType=JdbcType.VARCHAR),
        @Result(column="airport_name", property="airportName", jdbcType=JdbcType.VARCHAR),
        @Result(column="status", property="status", jdbcType=JdbcType.INTEGER)
    })
    Airport selectByPrimaryKey(String iataCode);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Airport
     *
     * @mbg.generated
     */
    @UpdateProvider(type=AirportSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Airport record, @Param("example") AirportExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Airport
     *
     * @mbg.generated
     */
    @UpdateProvider(type=AirportSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Airport record, @Param("example") AirportExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Airport
     *
     * @mbg.generated
     */
    @UpdateProvider(type=AirportSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Airport record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Airport
     *
     * @mbg.generated
     */
    @Update({
        "update Airport",
        "set city_code = #{cityCode,jdbcType=VARCHAR},",
          "airport_name = #{airportName,jdbcType=VARCHAR},",
          "status = #{status,jdbcType=INTEGER}",
        "where IATA_code = #{iataCode,jdbcType=CHAR}"
    })
    int updateByPrimaryKey(Airport record);
}