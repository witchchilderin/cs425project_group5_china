package com.route.dao.mapper;

import com.route.dao.po.Customer;
import com.route.dao.po.CustomerExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface CustomerMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @SelectProvider(type=CustomerSqlProvider.class, method="countByExample")
    long countByExample(CustomerExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @DeleteProvider(type=CustomerSqlProvider.class, method="deleteByExample")
    int deleteByExample(CustomerExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @Delete({
        "delete from Customer",
        "where customer_id = #{customerId,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer customerId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @Insert({
        "insert into Customer (customer_id, last_name, ",
        "first_name, ssn, driving_num, ",
        "passport, email, ",
        "phone, password, ",
        "home_airport, gender, ",
        "birth, description)",
        "values (#{customerId,jdbcType=INTEGER}, #{lastName,jdbcType=VARCHAR}, ",
        "#{firstName,jdbcType=VARCHAR}, #{ssn,jdbcType=BIGINT}, #{drivingNum,jdbcType=BIGINT}, ",
        "#{passport,jdbcType=VARCHAR}, #{email,jdbcType=VARCHAR}, ",
        "#{phone,jdbcType=VARCHAR}, #{password,jdbcType=VARCHAR}, ",
        "#{homeAirport,jdbcType=VARCHAR}, #{gender,jdbcType=INTEGER}, ",
        "#{birth,jdbcType=DATE}, #{description,jdbcType=LONGVARCHAR})"
    })
    int insert(Customer record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @InsertProvider(type=CustomerSqlProvider.class, method="insertSelective")
    int insertSelective(Customer record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @SelectProvider(type=CustomerSqlProvider.class, method="selectByExampleWithBLOBs")
    @Results({
        @Result(column="customer_id", property="customerId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="last_name", property="lastName", jdbcType=JdbcType.VARCHAR),
        @Result(column="first_name", property="firstName", jdbcType=JdbcType.VARCHAR),
        @Result(column="ssn", property="ssn", jdbcType=JdbcType.BIGINT),
        @Result(column="driving_num", property="drivingNum", jdbcType=JdbcType.BIGINT),
        @Result(column="passport", property="passport", jdbcType=JdbcType.VARCHAR),
        @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
        @Result(column="phone", property="phone", jdbcType=JdbcType.VARCHAR),
        @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
        @Result(column="home_airport", property="homeAirport", jdbcType=JdbcType.VARCHAR),
        @Result(column="gender", property="gender", jdbcType=JdbcType.INTEGER),
        @Result(column="birth", property="birth", jdbcType=JdbcType.DATE),
        @Result(column="description", property="description", jdbcType=JdbcType.LONGVARCHAR)
    })
    List<Customer> selectByExampleWithBLOBs(CustomerExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @SelectProvider(type=CustomerSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="customer_id", property="customerId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="last_name", property="lastName", jdbcType=JdbcType.VARCHAR),
        @Result(column="first_name", property="firstName", jdbcType=JdbcType.VARCHAR),
        @Result(column="ssn", property="ssn", jdbcType=JdbcType.BIGINT),
        @Result(column="driving_num", property="drivingNum", jdbcType=JdbcType.BIGINT),
        @Result(column="passport", property="passport", jdbcType=JdbcType.VARCHAR),
        @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
        @Result(column="phone", property="phone", jdbcType=JdbcType.VARCHAR),
        @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
        @Result(column="home_airport", property="homeAirport", jdbcType=JdbcType.VARCHAR),
        @Result(column="gender", property="gender", jdbcType=JdbcType.INTEGER),
        @Result(column="birth", property="birth", jdbcType=JdbcType.DATE)
    })
    List<Customer> selectByExample(CustomerExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @Select({
        "select",
        "customer_id, last_name, first_name, ssn, driving_num, passport, email, phone, ",
        "password, home_airport, gender, birth, description",
        "from Customer",
        "where customer_id = #{customerId,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="customer_id", property="customerId", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="last_name", property="lastName", jdbcType=JdbcType.VARCHAR),
        @Result(column="first_name", property="firstName", jdbcType=JdbcType.VARCHAR),
        @Result(column="ssn", property="ssn", jdbcType=JdbcType.BIGINT),
        @Result(column="driving_num", property="drivingNum", jdbcType=JdbcType.BIGINT),
        @Result(column="passport", property="passport", jdbcType=JdbcType.VARCHAR),
        @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR),
        @Result(column="phone", property="phone", jdbcType=JdbcType.VARCHAR),
        @Result(column="password", property="password", jdbcType=JdbcType.VARCHAR),
        @Result(column="home_airport", property="homeAirport", jdbcType=JdbcType.VARCHAR),
        @Result(column="gender", property="gender", jdbcType=JdbcType.INTEGER),
        @Result(column="birth", property="birth", jdbcType=JdbcType.DATE),
        @Result(column="description", property="description", jdbcType=JdbcType.LONGVARCHAR)
    })
    Customer selectByPrimaryKey(Integer customerId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @UpdateProvider(type=CustomerSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Customer record, @Param("example") CustomerExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @UpdateProvider(type=CustomerSqlProvider.class, method="updateByExampleWithBLOBs")
    int updateByExampleWithBLOBs(@Param("record") Customer record, @Param("example") CustomerExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @UpdateProvider(type=CustomerSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Customer record, @Param("example") CustomerExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @UpdateProvider(type=CustomerSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Customer record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @Update({
        "update Customer",
        "set last_name = #{lastName,jdbcType=VARCHAR},",
          "first_name = #{firstName,jdbcType=VARCHAR},",
          "ssn = #{ssn,jdbcType=BIGINT},",
          "driving_num = #{drivingNum,jdbcType=BIGINT},",
          "passport = #{passport,jdbcType=VARCHAR},",
          "email = #{email,jdbcType=VARCHAR},",
          "phone = #{phone,jdbcType=VARCHAR},",
          "password = #{password,jdbcType=VARCHAR},",
          "home_airport = #{homeAirport,jdbcType=VARCHAR},",
          "gender = #{gender,jdbcType=INTEGER},",
          "birth = #{birth,jdbcType=DATE},",
          "description = #{description,jdbcType=LONGVARCHAR}",
        "where customer_id = #{customerId,jdbcType=INTEGER}"
    })
    int updateByPrimaryKeyWithBLOBs(Customer record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Customer
     *
     * @mbg.generated
     */
    @Update({
        "update Customer",
        "set last_name = #{lastName,jdbcType=VARCHAR},",
          "first_name = #{firstName,jdbcType=VARCHAR},",
          "ssn = #{ssn,jdbcType=BIGINT},",
          "driving_num = #{drivingNum,jdbcType=BIGINT},",
          "passport = #{passport,jdbcType=VARCHAR},",
          "email = #{email,jdbcType=VARCHAR},",
          "phone = #{phone,jdbcType=VARCHAR},",
          "password = #{password,jdbcType=VARCHAR},",
          "home_airport = #{homeAirport,jdbcType=VARCHAR},",
          "gender = #{gender,jdbcType=INTEGER},",
          "birth = #{birth,jdbcType=DATE}",
        "where customer_id = #{customerId,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Customer record);
}