package com.route.entity.request;

import java.util.Date;

/**
 * @Auther: wuwei
 * @Date: 2021/5/12 11:12
 * @Description: 注册信息请求实体
 */
public class RegisterInfoRequestEntity {

    private String lastName;//名
    private String firstName;//姓
    private String email;//邮箱
    private String password;//密码

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
