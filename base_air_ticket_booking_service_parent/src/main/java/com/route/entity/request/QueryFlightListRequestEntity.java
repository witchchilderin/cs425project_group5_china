package com.route.entity.request;

/**
 * @Date: 2021/5/14 20:32
 * @Description: Query flight list data query param entity.
 */
public class QueryFlightListRequestEntity {

    private String departureAirpot;//起飞机场
    private String arrivalAirport;//到达机场

    private String departureTime;//起飞时间
    private String arrivalTime;//到达时间

    private int pageNum;
    private int pageSize;

    public String getDepartureAirpot() {
        return departureAirpot;
    }

    public void setDepartureAirpot(String departureAirpot) {
        this.departureAirpot = departureAirpot;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
