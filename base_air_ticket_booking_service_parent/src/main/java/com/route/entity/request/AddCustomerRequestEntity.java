package com.route.entity.request;

import java.util.Date;

/**
 * @Auther: WeiWU
 * @Date: 2021/5/12 10:46
 * @Description: 添加用户请求参数实体
 */
public class AddCustomerRequestEntity {

    private Integer customerId;//客户编号
    private String lastName;//名
    private String firstName;//姓
    private Long ssn;//社保号
    private Long drivingNum;//驾驶证号
    private String passport;//护照
    private String email;//邮箱
    private String phone;//电话
    private String password;//密码
    private String homeAirport;//机场
    private Integer gender;//性别
    private Date birth;//生日
    private String description;//描述

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Long getSsn() {
        return ssn;
    }

    public void setSsn(Long ssn) {
        this.ssn = ssn;
    }

    public Long getDrivingNum() {
        return drivingNum;
    }

    public void setDrivingNum(Long drivingNum) {
        this.drivingNum = drivingNum;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHomeAirport() {
        return homeAirport;
    }

    public void setHomeAirport(String homeAirport) {
        this.homeAirport = homeAirport;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
