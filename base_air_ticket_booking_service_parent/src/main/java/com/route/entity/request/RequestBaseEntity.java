package com.route.entity.request;

/**
 * @Auther: wuwei
 * @Date: 2020/7/3 16:07
 * @Description:基础请求参数实体
 */
public class RequestBaseEntity {

    private String userId;//用户id
    private String token;//用户登录token

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
