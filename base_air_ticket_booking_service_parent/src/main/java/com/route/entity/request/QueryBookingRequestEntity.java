package com.route.entity.request;

/**
 * @Auther: WeiWU
 * @Date: 2021/5/11 10:55
 * @Description: 查询预订订单请求参数实体
 */
public class QueryBookingRequestEntity {

    private String orderId;//订单编号
    private Integer customerId;//客户编号
    private Integer status;//状态  0:可飞 1停飞
    private String departureTime;//起飞时间
    private String arrivalTime;//到达时间
    private String departureCity;//起飞城市
    private String arrivalCity;//到达城市
    private String creationTime;//创建日期
    private Long totalPriceStart;//起始总价格
    private Long totalPriceEnd;//结束总价格

    private int pageNum;
    private int pageSize;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public Long getTotalPriceStart() {
        return totalPriceStart;
    }

    public void setTotalPriceStart(Long totalPriceStart) {
        this.totalPriceStart = totalPriceStart;
    }

    public Long getTotalPriceEnd() {
        return totalPriceEnd;
    }

    public void setTotalPriceEnd(Long totalPriceEnd) {
        this.totalPriceEnd = totalPriceEnd;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
