package com.route.entity.response;

import java.util.List;

/**
 * @Auther: wuwei
 * @Date: 2020/6/24 16:43
 * @Description:响应通用实体
 */
public class ResponseEntity {

    //是否成功 000000表示成功 1xxxxx表示系统预定义响应码 其余属于接口自定义
    private Integer responseCode;
    //返回消息
    private String message;
    //集合数据
    private List<?> dataList;
    //单个实体数据
    private Object objEntity;

    //成功默认消息
    private static final String SUCCESS_MESSAGE = "success";
    //失败默认消息
    private static final String FAIL_MESSAGE = "fail";
    //成功响应编码
    private static final Integer SUCCESS_CODE = 000000;
    //失败响应编码
    private static final Integer FAIL_CODE = 100000;

    //构建成功实体
    public static ResponseEntity getSuccess(String message){
        ResponseEntity responseEntity = new ResponseEntity();
        responseEntity.responseCode = SUCCESS_CODE;//设置成功true
        responseEntity.setMessage(message);//设置消息
        return responseEntity;
    }

    //构建成功实体(集合数据)
    public static ResponseEntity getSuccessByListData(String message, List<?> list){
        ResponseEntity responseEntity = new ResponseEntity();
        responseEntity.responseCode = SUCCESS_CODE;//设置成功true
        responseEntity.dataList = list;//集合数据
        responseEntity.setMessage(message);//设置消息
        return responseEntity;
    }

    //构建成功实体(单实体数据)
    public static ResponseEntity getSuccessByEntity(String message, Object objEntity){
        ResponseEntity responseEntity = new ResponseEntity();
        responseEntity.responseCode = SUCCESS_CODE;//设置成功true
        responseEntity.objEntity = objEntity;//单实体数据
        //设置响应消息
        if(message == null || "".equals(message)){
            responseEntity.message = SUCCESS_MESSAGE;
        }else{
            responseEntity.message = message;
        }
        return responseEntity;
    }

    //构建失败实体
    public static ResponseEntity getFail(String message){
        ResponseEntity responseEntity = new ResponseEntity();
        responseEntity.responseCode = FAIL_CODE;//设置失败false
        //设置响应消息
        if(message == null || "".equals(message)){
            responseEntity.message = FAIL_MESSAGE;
        }else{
            responseEntity.message = message;
        }
        return responseEntity;
    }

    //构建失败实体
    public static ResponseEntity getFailAndCode(String message, Integer responseCode){
        //获取失败响应实体
        ResponseEntity responseEntity = getFail(message);
        //设置响应码
        if(responseCode == null){
            responseEntity.responseCode = FAIL_CODE;//设置失败false
        }else{
            responseEntity.responseCode = responseCode;//设置失败false
        }
        return responseEntity;
    }

    /**
     * 判断响应是否成功
     * 功能描述:判断响应是否成功
     * @return: boolean true false
     * @auther: wuwei
     * @date: 2020/7/2 16:26
     */
    public boolean isSuccess(){
        if(this.responseCode == SUCCESS_CODE){
            return true;
        }else{
            return false;
        }
    }

    //get set方法
    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<?> getDataList() {
        return dataList;
    }

    public void setDataList(List<?> dataList) {
        this.dataList = dataList;
    }

    public Object getObjEntity() {
        return objEntity;
    }

    public void setObjEntity(Object objEntity) {
        this.objEntity = objEntity;
    }
}
