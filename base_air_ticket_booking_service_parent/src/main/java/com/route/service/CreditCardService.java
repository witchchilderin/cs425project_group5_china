package com.route.service;

import com.route.dao.mapper.CreditcardMapper;
import com.route.dao.po.Creditcard;
import com.route.dao.po.CreditcardExample;
import com.route.entity.response.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Date: 2021/5/13 11:08
 * @Description: CreditCard service class.
 */
@Service
public class CreditCardService {

    @Resource
    private CreditcardMapper creditcardMapper;

    /**
     * @Description: Query creditcard info by customerId
     * @param: customerId
     * @return: ResponseEntity
     * @date: 2021/5/13 11:17
     */
    public ResponseEntity queryCreditCardListByCustomerId(Integer customerId){
        //参数检查
        if(customerId == null){
            return ResponseEntity.getFail("The customerId is null!");
        }
        //实例化查询条件类
        CreditcardExample example = new CreditcardExample();
        example.createCriteria().andCustomerIdEqualTo(customerId);
        //执行查询
        List<Creditcard> creditcardList = creditcardMapper.selectByExample(example);
        //构建响应实体并返回
        return ResponseEntity.getSuccessByListData("success",creditcardList);
    }

}
