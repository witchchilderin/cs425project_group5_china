package com.route.service;

import com.github.pagehelper.PageHelper;
import com.route.dao.mapper.FlightMapper;
import com.route.dao.po.Flight;
import com.route.dao.po.FlightExample;
import com.route.entity.request.QueryFlightListRequestEntity;
import com.route.entity.response.ResponseEntity;
import com.route.util.DateUtil;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @Date: 2021/5/14 20:29
 * @Description: Flight Service Class
 */
@Service
public class FlightService {

    @Resource
    private FlightMapper flightMapper;

    public ResponseEntity queryFlightListByPage(QueryFlightListRequestEntity requestEntity){
        FlightExample example = new FlightExample();
        FlightExample.Criteria criteria = null;
        if(requestEntity != null){
            criteria = example.createCriteria();
            //起飞城市
            if(requestEntity.getDepartureAirpot() != null && !("".equals(requestEntity.getDepartureAirpot()))){
                criteria.andDepartureAirpotEqualTo(requestEntity.getDepartureAirpot());
            }
            //到达城市
            if(requestEntity.getArrivalAirport() != null && !("".equals(requestEntity.getArrivalAirport()))){
                criteria.andArrivalAirportEqualTo(requestEntity.getArrivalAirport());
            }
            //起飞时间
            if(requestEntity.getDepartureTime() != null && !("".equals(requestEntity.getDepartureTime()))){
                criteria.andDepartureTimeLessThanOrEqualTo(DateUtil.getDateFormat(requestEntity.getDepartureTime()));
            }
            //到达时间
            if(requestEntity.getArrivalTime() != null && !("".equals(requestEntity.getArrivalTime()))){
                criteria.andArrivalTimeGreaterThanOrEqualTo(DateUtil.getDateFormat(requestEntity.getArrivalTime()));
            }
        }
        if(requestEntity.getPageNum() > 0 && requestEntity.getPageSize() > 0) {
            PageHelper.startPage(requestEntity.getPageNum(), requestEntity.getPageSize());
        }
        List<Flight> list = flightMapper.selectByExample(example);
        return ResponseEntity.getSuccessByListData("success",list);
    }

}
