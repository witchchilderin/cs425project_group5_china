package com.route.service;

import com.github.pagehelper.PageHelper;
import com.route.dao.mapper.BookingMapper;
import com.route.dao.mapper.BookingdetailMapper;
import com.route.dao.po.Booking;
import com.route.dao.po.BookingExample;
import com.route.entity.request.QueryBookingRequestEntity;
import com.route.util.DateUtil;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @Auther: WeiWU
 * @Date: 2021/5/11 10:44
 * @Description: Booking service class.
 */
@Service
public class BookingService {

    @Resource
    private BookingMapper bookingMapper;
    @Resource
    private BookingdetailMapper bookingdetailMapper;

    /**
     * @Description: Query booking info
     * @param: String orderId
     * @param: Integer customerId
     * @param: Integer status
     * @param: String departureTime
     * @param: String arrivalTime
     * @param: String departureCity
     * @param: String arrivalCity
     * @param: String creationTime
     * @param: long totalPriceStart
     * @param: long totalPriceEnd
     * @return: ResponseEntity
     * @date: 2021/5/11 11:08
     */
    public List<Booking> queryBookingListByPage(Integer customerId,QueryBookingRequestEntity requestEntity){
        BookingExample example = createQueryExample(customerId,requestEntity);
        PageHelper.startPage(requestEntity.getPageNum(),requestEntity.getPageSize());
        List<Booking> list = bookingMapper.selectByExample(example);
        return list;
    }

    /**
     * @Description: Count booking data number
     * @param: String orderId
     * @param: Integer customerId
     * @param: Integer status
     * @param: String departureTime
     * @param: String arrivalTime
     * @param: String departureCity
     * @param: String arrivalCity
     * @param: String creationTime
     * @param: long totalPriceStart
     * @param: long totalPriceEnd
     * @return: data number
     * @date: 2021/5/11 11:08
     */
    public long countBookingTotal(Integer customerId,QueryBookingRequestEntity requestEntity){
        BookingExample example = createQueryExample(customerId,requestEntity);
        long count = bookingMapper.countByExample(example);
        return count;
    }

    /**
     * @Description: Create bookingExample class
     * @param: String orderId
     * @param: Integer customerId
     * @param: Integer status
     * @param: String departureTime
     * @param: String arrivalTime
     * @param: String departureCity
     * @param: String arrivalCity
     * @param: String creationTime
     * @param: long totalPriceStart
     * @param: long totalPriceEnd
     * @return: BookingExample
     * @date: 2021/5/11 11:08
     */
    private BookingExample createQueryExample(Integer customerId,QueryBookingRequestEntity requestEntity){
        //实例化查询条件实例
        BookingExample example = new BookingExample();
        BookingExample.Criteria criteria = null;
        if(requestEntity != null){
            criteria = example.createCriteria();
            //customerId
            if(customerId != null && customerId >= 0){
                criteria.andCustomerIdEqualTo(customerId);
            }
            //订单编号
            if(requestEntity.getOrderId() != null && !("".equals(requestEntity.getOrderId()))){
                criteria.andOrderIdEqualTo(requestEntity.getOrderId());
            }
            //客户编号
            if(requestEntity.getCustomerId() != null && !("".equals(requestEntity.getCustomerId()))){
                criteria.andCustomerIdEqualTo(requestEntity.getCustomerId());
            }
            //状态  0:可飞 1停飞
            if(requestEntity.getStatus() != null && !(requestEntity.getStatus() == 0 || requestEntity.getStatus() == 1)){
                criteria.andStatusEqualTo(requestEntity.getStatus());
            }
            //起飞时间
            if(requestEntity.getDepartureTime() != null && !("".equals(requestEntity.getDepartureTime()))){
                criteria.andDepartureTimeGreaterThanOrEqualTo(DateUtil.getDateFormat(requestEntity.getDepartureTime()));
            }
            //到达时间
            if(requestEntity.getArrivalTime() != null && !("".equals(requestEntity.getArrivalTime()))){
                criteria.andArrivalTimeLessThanOrEqualTo(DateUtil.getDateFormat(requestEntity.getArrivalTime()));
            }
            //起飞城市
            if(requestEntity.getDepartureCity() != null && !("".equals(requestEntity.getDepartureCity()))){
                criteria.andDepartureCityEqualTo(requestEntity.getDepartureCity());
            }
            //到达城市
            if(requestEntity.getArrivalCity() != null && !("".equals(requestEntity.getArrivalCity()))){
                criteria.andArrivalCityEqualTo(requestEntity.getArrivalCity());
            }
            //创建日期
            if(requestEntity.getCreationTime() != null && !("".equals(requestEntity.getCreationTime()))){
                criteria.andCreationTimeEqualTo(DateUtil.getDateFormat(requestEntity.getCreationTime()));
            }
            //起始总价格
            if(requestEntity.getTotalPriceStart() != null && requestEntity.getTotalPriceStart() > 0){
                criteria.andTotalPriceGreaterThanOrEqualTo(requestEntity.getTotalPriceStart());
            }
            //结束总价格
            if(requestEntity.getTotalPriceEnd() != null && requestEntity.getTotalPriceEnd() > 0){
                criteria.andTotalPriceLessThanOrEqualTo(requestEntity.getTotalPriceEnd());
            }
        }
        return example;
    }

}
