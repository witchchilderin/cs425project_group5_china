package com.route.service;

import com.route.dao.mapper.BookingMapper;
import com.route.dao.po.Booking;
import com.route.entity.response.ResponseEntity;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @Date: 2021/5/13 10:32
 * @Description: Pay service class.
 */
@Service
public class PayService {

    @Resource
    private BookingMapper bookingMapper;

    /**
     * @Description: Do pay service
     * @param: customerId
     * @param: orderId
     * @return: ResponseEntity
     * @date: 2021/5/14 12:34
     */
    public ResponseEntity doPay(Integer customerId, String orderId){
        //实例化Booking参数实例
        Booking booking = new Booking();
        booking.setCustomerId(customerId);
        booking.setOrderId(orderId);
        booking.setPayStatus(1);//支付状态为成功
        //执行修改操作
        int count = bookingMapper.updateByPrimaryKeySelective(booking);
        //判断受影响行数
        if(count > 0){
            //返回成功
            return ResponseEntity.getSuccess("success");
        }else{
            //返回失败
            return ResponseEntity.getFail("success");
        }
    }

}
