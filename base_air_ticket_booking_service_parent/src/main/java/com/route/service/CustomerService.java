package com.route.service;

import com.route.dao.mapper.CustomerMapper;
import com.route.dao.po.Customer;
import com.route.entity.request.AddCustomerRequestEntity;
import com.route.entity.request.RegisterInfoRequestEntity;
import com.route.entity.request.UpdateCustomerInfoRequestEntity;
import com.route.entity.response.ResponseEntity;
import com.route.util.DateUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

/**
 * @Date: 2021/5/12 10:39
 * @Description: Customer service class.
 */
@Service
public class CustomerService {

    @Resource
    private CustomerMapper customerMapper;

    /**
     * @Description: Register a customer
     * @param: String lastName
     * @param: String firstName
     * @param: String email
     * @param: String password
     * @return: ResponseEntity
     * @date: 2021/5/12 11:22
     */
    public ResponseEntity register(RegisterInfoRequestEntity requestEntity){
        //参数验证
        if(requestEntity == null){
            return ResponseEntity.getFail("Please enter the registration data!");
        }
        if(requestEntity.getEmail() == null || "".equals(requestEntity.getEmail())){
            return ResponseEntity.getFail("Email is empty!");
        }
        if(requestEntity.getFirstName() == null || "".equals(requestEntity.getFirstName())){
            return ResponseEntity.getFail("FirstName is empty!");
        }
        if(requestEntity.getLastName() == null || "".equals(requestEntity.getLastName())){
            return ResponseEntity.getFail("LastName is empty!");
        }
        if(requestEntity.getPassword() == null || "".equals(requestEntity.getPassword())){
            return ResponseEntity.getFail("Password is empty!");
        }
        //实例化添加用户请求实体
        AddCustomerRequestEntity addRequestEntity = new AddCustomerRequestEntity();
        addRequestEntity.setLastName(requestEntity.getLastName());//名
        addRequestEntity.setFirstName(requestEntity.getFirstName());//姓
        addRequestEntity.setEmail(requestEntity.getEmail());//邮箱
        addRequestEntity.setPassword(requestEntity.getPassword());//密码
        //调用添加用户方法
        int count = this.addCustomer(addRequestEntity);
        //判断是否添加成功
        if(count > 0){
            //返回成功
            return ResponseEntity.getSuccess("success");
        }else{
            //返回失败
            return ResponseEntity.getFail("fail");
        }
    }

    public int addCustomer(AddCustomerRequestEntity requestEntity){
        //实例化用户实体类
        Customer customer = new Customer();
        customer.setCustomerId(requestEntity.getCustomerId());//客户编号
        customer.setLastName(requestEntity.getLastName());//名
        customer.setFirstName(requestEntity.getFirstName());//姓
        customer.setSsn(requestEntity.getSsn());//社保号
        customer.setDrivingNum(requestEntity.getDrivingNum());//驾驶证号
        customer.setPassport(requestEntity.getPassport());//护照
        customer.setEmail(requestEntity.getEmail());//邮箱
        customer.setPhone(requestEntity.getPhone());//电话
        customer.setPassword(requestEntity.getPassword());//密码
        customer.setHomeAirport(requestEntity.getHomeAirport());//机场
        customer.setGender(requestEntity.getGender());//性别
        customer.setBirth(requestEntity.getBirth());//生日
        customer.setDescription(requestEntity.getDescription());//描述
        //添加用户信息
        int count = customerMapper.insertSelective(customer);
        return count;
    }

    public ResponseEntity queryCustomerInfoByCustomerId(Integer customerId){
        //参数验证
        if(customerId == null || customerId <= 0){
            return ResponseEntity.getFail("The customerId is null!");
        }
        Customer customer = customerMapper.selectByPrimaryKey(customerId);
        return ResponseEntity.getSuccessByEntity("success",customer);
    }

    public ResponseEntity updateCustomerInfoByCustomerId(UpdateCustomerInfoRequestEntity requestEntity){
        //参数验证
        if(requestEntity == null){
            return ResponseEntity.getFail("The request param is null!");
        }
        if(requestEntity.getCustomerId() == null || requestEntity.getCustomerId() <= 0){
            return ResponseEntity.getFail("The customerId is null!");
        }
        Customer customer = new Customer();
        customer.setCustomerId(requestEntity.getCustomerId());
        customer.setLastName(requestEntity.getLastName());
        customer.setFirstName(requestEntity.getFirstName());
        customer.setSsn(requestEntity.getSsn());
        customer.setDrivingNum(requestEntity.getDrivingNum());
        customer.setPassport(requestEntity.getPassport());
        customer.setEmail(requestEntity.getEmail());
        customer.setPhone(requestEntity.getPhone());
        customer.setPassword(requestEntity.getPassword());
        customer.setHomeAirport(requestEntity.getHomeAirport());
        customer.setGender(requestEntity.getGender());
        customer.setBirth(DateUtil.getDateFormat(requestEntity.getBirth()));
        customer.setDescription(requestEntity.getDescription());
        int count = customerMapper.updateByPrimaryKeySelective(customer);
        if(count > 0){
            //返回成功
            return ResponseEntity.getSuccess("success");
        }else{
            //返回失败
            return ResponseEntity.getFail("fail");
        }
    }

}
